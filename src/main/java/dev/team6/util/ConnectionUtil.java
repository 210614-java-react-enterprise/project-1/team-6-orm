package dev.team6.util;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConnectionUtil {
    private static Connection connection;

    private static final boolean IS_TEST = Boolean.parseBoolean(System.getenv("TEST"));

    public static Connection getConnection() throws SQLException {

        //jdbc connection string format:  jdbc:postgresql://host:port/database

        if(connection == null || connection.isClosed()){
            if(IS_TEST){
                connection = DriverManager.getConnection("jdbc:h2:~/test");
            }
            else {
//                String url = "jdbc:postgresql://project-0.cxpt56xtzfsi.us-east-2.rds.amazonaws.com:5432/postgres";
                final String URL = System.getenv("JDBC_CONNECTION_STRING");
                final String PASSWORD = System.getenv("PASSWORD");
                final String USERNAME = System.getenv("USERNAME");
                if(connection==null || connection.isClosed()) {
                    connection = DriverManager.getConnection(URL, USERNAME, PASSWORD);
//                    connection.setSchema("Project1");
                }
            }
        }
       // System.out.println(connection.getMetaData().getDriverName());   ///to verify whether connected to db
        return connection;
    }

}
