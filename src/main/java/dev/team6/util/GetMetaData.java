package dev.team6.util;
/* Carlos Galvan Jr
This getMetaData class purpose is to hold utility methods for the DOA implemetation
Such util methods include:
generateUpdateSql(): returns a String of a prepared statement sql query
getGetter(): this method takes a field and a class object and finds the right getter funtion and then returns the invoked getter (this
            was used in the update part of the DAO implementation)

 */

import java.beans.IntrospectionException;
import java.beans.PropertyDescriptor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Map;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicReference;

public class GetMetaData {

    //generates a sql prepared statement
    //to preserve the integrity of data, the selected column is ommited from the update field
    public <T> String generateUpdateSQL(T object, String selectedColumn, T value) {
        String primaryKey = selectedColumn;
        Class<?> objectClass = object.getClass();
        StringBuffer sql = new StringBuffer();
        sql.append("UPDATE " + objectClass.getSimpleName().toLowerCase() + " SET ");
        Field[] fields = objectClass.getDeclaredFields();
        AtomicBoolean verify = new AtomicBoolean(false);
        boolean finalVerify = verify.get();
        Arrays.stream(fields).forEach(f -> {
            if (!(f.getName().toLowerCase().equals(primaryKey.toLowerCase()))) {
                sql.append(f.getName() + "=?, ");
            }
        });

        sql.setLength(sql.length() - 2);
        if (value instanceof Integer) {
            sql.append(" WHERE " + selectedColumn + " = " + value + ";");
        } else {
            sql.append(" WHERE " + selectedColumn + " = \'" + value + "\';");

        }

        return sql.toString();
    }

    public  <T> Object getGetter(Field field, T object) {
        // MZ: Find the correct method
        for (Method m : object.getClass().getMethods()) {
            if ((m.getName().startsWith("get")) && (m.getName().length() == (field.getName().length() + 3))) {
                if (m.getName().toLowerCase().endsWith(field.getName().toLowerCase())) {
                    // MZ: Method found, run it
                    try {
                        return m.invoke(object);
                    } catch (IllegalAccessException e) {
                        e.printStackTrace();
                    } catch (InvocationTargetException e) {
                        e.printStackTrace();
                    }

                }
            }
        }


        return null;
    }

}
