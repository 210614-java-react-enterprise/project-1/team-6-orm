package dev.team6.daos;

import dev.team6.util.ConnectionUtil;

import java.lang.reflect.Field;
import java.sql.Connection;
import java.sql.Statement;
import java.util.Locale;

public class DataBaseDaoImpl implements DataBaseDao{

    ////---------------------createTable creates a new table in the DB, taking in the object--------------------------------///////
    @Override
    public <T> void createTable(T object){
        Class<?> objectClass = object.getClass();
        String tableName = object.getClass().getSimpleName().toLowerCase();
        Field[] fields = objectClass.getDeclaredFields();
        int noOfColumns = fields.length;
        String sql = "Create Table IF NOT EXISTS " +tableName+ " ( ";
        sql = sql + "id serial primary key, ";
        for(int i=1; i<noOfColumns; i++){
            String columnName = fields[i].getName();
            final Class<?> type = fields[i].getType();
            String colType = type.getTypeName();
            String columnType;
            columnType = columnTypeName(colType);
            sql = sql + columnName + " " + columnType + ", ";
        }
        sql = sql.replaceAll(", $", "");
        sql = sql + ");";
        try{
            Connection connection = ConnectionUtil.getConnection();
            Statement statement = connection.createStatement();
            statement.executeUpdate(sql);
            System.out.println("Table create successfully");
        }catch(Exception e){
            System.out.println(("Error creating table: " +e.getMessage()));
            // System.exit(0);
        }
    }

    /////---------------------columnTypeName converts java datatypes to postgres data types-----------------------------------/////
    public String columnTypeName(String colType){
        String column = "";
        switch(colType){
            case "int":
                column = "int";
                break;
            case "byte":
                column =  "int";
                break;
            case "short":
                column =  "int";
                break;
            case "long":
                column =  "bigint";
                break;
            case "float":
                column =  "decimal";
                break;
            case "double":
                column =  "decimal";
                break;
            case "java.lang.String":
                column =  "text";
                break;
            case "java.lang.Character":
                column =  "text";
                break;
            case "java.time.LocalDate":
                column =  "date";
                break;
            case "java.time.LocalDateTime":
                column =  "timestamp";
                break;
            case "java.time.LocalTime":
                column =  "time";
                break;
        }
        return column;
    }

    /////------------------------------------dropTable drops the table--------------------------------------/////////
    @Override
    public <T> boolean dropTable(T object) {
        String tableName = object.getClass().getSimpleName();
        String sql = "Drop Table " + tableName;
        try{
            Connection connection = ConnectionUtil.getConnection();
            Statement statement = connection.createStatement();
            statement.executeUpdate(sql);
            System.out.println("Table '" + tableName.toLowerCase()+ "' dropped successfully");
            return true;
        }catch(Exception e){
            System.out.println(("Error creating table: " +e.getMessage()));
            return  false;
            // System.exit(0);
        }
    }

}
