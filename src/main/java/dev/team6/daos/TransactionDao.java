package dev.team6.daos;

import java.util.List;

public interface TransactionDao {
    ///----------------------------All DML(CRUD) operations are added in this interface----------------------------------------------////////


    //Find all returns a list of objects
    <T> List<T> findAll(T object);
    <T> List<T> findAllByField(T object, boolean pojo, String selectedColumn, T parameter);
    //Returns an object based on given parameters (or null)
    <T> Object findOne(T object, String primaryKeyColumn, T primaryKey );
    <T> boolean update(T object, String columnName, T value, int id);
    <T> boolean updateByField(T object,String columnName, T value);
    //Read but just one via a specified criteria
    <T> boolean delete(T object, String columnName, T value);
    <T> int createRecord(T object);

    
}
