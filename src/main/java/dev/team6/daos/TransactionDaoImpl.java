package dev.team6.daos;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.SerializationFeature;
import dev.team6.exceptions.JsonMappingException;
import dev.team6.util.ConnectionUtil;
import com.fasterxml.jackson.databind.ObjectMapper;
import dev.team6.util.GetMetaData;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.sql.*;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

public class TransactionDaoImpl implements TransactionDao {


    /////---------createRecord is called from the main method. Takes the class of the particular object and check to see if the corresponding table is available in the DB.--////
    /////---------if table exists, it will add the record to the table, otherwise creates a new table with class name and add the record to the table----------------------//////
    public <T> int createRecord(T object) {
        DataBaseDao dbDao = new DataBaseDaoImpl();
        Class<?> objectClass = object.getClass();
        String tableName = objectClass.getSimpleName().toLowerCase();
        try {
            Connection connection = ConnectionUtil.getConnection();
            DatabaseMetaData dbm = connection.getMetaData();
            ResultSet tables = dbm.getTables(null, null, tableName, null);
            if (tables.next()) {
                System.out.println("table exists");
                return addRecord(object, tableName);
            } else {
                System.out.println("Table doesnot exists");
                dbDao.createTable(object);
                return addRecord(object, tableName);
            }
        } catch (Exception e) {
            System.out.println(("Error creating table: " + e.getMessage()));
        }
        return 0;
    }


    ////----------addRecord insert the record to the Table----(CREATE operation)-------------------------/////
    public <T> int addRecord(T object, String tableName) {
        Class<?> objectClass = object.getClass();
        Field[] fields = objectClass.getDeclaredFields();
        int noOfColumns = fields.length;
        String sql = "insert into " + tableName + " values ( default, ";
        for (int i = 1; i < noOfColumns; i++) {
            String fieldName = fields[i].getName();
            String getterName = "get" + fieldName.substring(0, 1).toUpperCase() + fieldName.substring(1);
            try {
                Method getterMethod = objectClass.getMethod(getterName);
                Object fieldValue = getterMethod.invoke(object);
                String columnType = fields[i].getType().getTypeName();
                if (columnType == "int") {
                    sql = sql + fieldValue + ", ";
                } else if (columnType == "java.lang.String" || columnType == "java.time.LocalTime" || columnType == "java.time.LocalDateTime" || columnType == "java.time.LocalDate" || columnType == "java.lang.Character") {
                    sql = sql + "'" + fieldValue + "'" + ", ";
                } else {
                    sql = sql + fieldValue + ", ";
                }
            } catch (NoSuchMethodException e) {
                throw new JsonMappingException("No suitable getter for: " + fieldName, e);
            } catch (IllegalAccessException e) {
                throw new JsonMappingException("Cannot access getter for: " + fieldName, e);
            } catch (InvocationTargetException e) {
                throw new JsonMappingException("Issue invoking getter for: " + fieldName, e);
            }
        }
        sql = sql.replaceAll(", $", "");
        sql = sql + ");";
        try (Connection connection = ConnectionUtil.getConnection();
             PreparedStatement ps = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);) {
            ps.executeUpdate();
            try (ResultSet keys = ps.getGeneratedKeys()) {
                keys.next();
                return keys.getInt(1);
            }
        } catch (Exception e) {
            System.out.println(("Error creating table: " + e.getMessage()));
            // System.exit(0);
        }
        return 0;
    }


    @Override
    public <T> List<T> findAll(T object) {
        Class<?> objectClass = object.getClass();
        List<T> findAllList = (List<T>) findAllByField(object, true, "", 0);
        if (findAllList.size() > 0) {
            return findAllList;
        }
        return null;
    }

    ///////---------------findAll select data from the table---(READ operation)---------------------------------------------/////
    /*
        findAll() : This method is designed to return an List of JSON objects of the database
        Jackson JSON is used to convert a POJO to JSON format
        - Carlos
     */
    @Override
    public <T> List<T> findAllByField(T object, boolean pojo, String selectedColumn, T parameter) {
        Class<?> objectClass = object.getClass();
        ObjectMapper mapper = new ObjectMapper(); //Jackson JSON object
        mapper.registerModule(new JavaTimeModule());
        mapper.disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);
        Field[] fields = objectClass.getDeclaredFields();
        String sql = "SELECT * FROM " + objectClass.getSimpleName().toLowerCase(); //Initial sql statement to get all data
        if (!selectedColumn.equals("")) {//if you leave the selectedColumn parameter empty then dont add a WHERE clause
//            sql += " WHERE " + selectedColumn + " = " + parameter;
            if (parameter instanceof Integer || parameter instanceof Double) {//for strings
                sql += " WHERE " + selectedColumn + " = " + parameter + ";";
            } else {
                sql += " WHERE " + selectedColumn + " = \'" + parameter + "\';";

            }
        }
        List<Map<String, Object>> resultSetMap = new ArrayList<>(); // this List will be used to contain JSON objects
        List<String> jsonList = new ArrayList<>(); // this List will be used to contain JSON objects
        try (Connection connection = ConnectionUtil.getConnection();
             Statement s = connection.createStatement();
             ResultSet rs = s.executeQuery(sql)
        ) {
            while (rs.next()) {
                ResultSetMetaData metaData = rs.getMetaData(); // Metadata used to get object type
                Map<String, Object> currentResultSetMap = new LinkedHashMap<>(); //Map used to get current ResultSet column name and its value
                AtomicInteger count = new AtomicInteger(1);
                Arrays.stream(fields).forEach(f -> {
                    try {
                        int type = metaData.getColumnType(count.get()); //this gets type based on the current row index
                        count.getAndIncrement();
                        if (type == Types.VARCHAR || type == Types.CHAR) {//for text and strings
                            currentResultSetMap.put(f.getName(), rs.getString(f.getName().toLowerCase()));
                        }
                        if (type == Types.NUMERIC) {//for doubles
                            currentResultSetMap.put(f.getName(), rs.getDouble(f.getName().toLowerCase()));
                        }
                        if (type == Types.INTEGER) { //for ints
                            currentResultSetMap.put(f.getName(), rs.getInt(f.getName().toLowerCase()));
                        }
                        if (type == Types.TIMESTAMP) { //for time (convert to localdate)
                            Time time = rs.getTime(f.getName().toLowerCase());
                            Timestamp timestamp = new Timestamp(time.getTime());
                            currentResultSetMap.put(f.getName(), timestamp.toLocalDateTime().toString());
                        }
                        if (type == Types.JAVA_OBJECT) {//for java objects
                            System.out.println(rs.getInt(f.getName()));
                            currentResultSetMap.put(f.getName(), rs.getObject(f.getName().toLowerCase()));
                        }
                    } catch (SQLException throwables) {
                        throwables.printStackTrace();
                    }
                });


                if (currentResultSetMap != null) {
                    String jsonResult = mapper.writerWithDefaultPrettyPrinter()
                            .writeValueAsString(currentResultSetMap);
//                    System.out.println(jsonResult);
                    jsonList.add(jsonResult);
                }

            }

            List<Object> returnList = jsonList.stream().map(m -> {
                try {
                    return mapper.readValue(m, objectClass);
                } catch (JsonProcessingException e) {
                    e.printStackTrace();
                }
                return null;
            }).collect(Collectors.toList());

            if (!pojo) {
                return (List<T>) jsonList;
            }
            return (List<T>) returnList;
        } catch (SQLException | JsonProcessingException throwables) {
            throwables.printStackTrace();
        }

        return null;
    }

    /*
           findOne() (Carlos Galvan Jr)
           This method purpose is to find a single object based on the given input
           input/parameters include the objectClass , the name of the primary key column and the primary key

     */
    @Override
    public <T> Object findOne(T object, String primaryKeyColumn, T primaryKey) {
        Class<?> objectClass = object.getClass();
        List<T> found = (List<T>) findAllByField(object, true, primaryKeyColumn, primaryKey);

        if (found.size() > 0) {
            return found.get(0);
        }
        return null;
    }

    /*
    updateByField() by carlos galvan jr
    This method purpose is to update an the corresponding objects' row data based on the input
    The input/parameters include the updated object, the selectedColumn (ideally primary key column) and the value (PK)
     */

    /////----------------------updateByField updates the data in the table----(UPDATE operation)-------------------------------------////////
    @Override
    public <T> boolean updateByField(T object, String selectedColumn, T value) {
        Class<?> objectClass = object.getClass();
        Field[] fields = objectClass.getDeclaredFields();
        GetMetaData md = new GetMetaData();
        String sql = md.generateUpdateSQL(object, selectedColumn, value);
        System.out.println(sql);
        AtomicInteger count = new AtomicInteger(1);
        try (Connection connection = ConnectionUtil.getConnection();
             PreparedStatement ps = connection.prepareStatement(sql);) {
            Arrays.stream(fields).forEach(f -> {
                String columnType = f.getType().getTypeName();
                if (!f.getName().toLowerCase().equals(selectedColumn)) {
                    if (columnType.matches("(.*)String")) {
                        try {
                            ps.setString(count.get(), (String) md.getGetter(f, object));
                        } catch (SQLException throwables) {
                            throwables.printStackTrace();
                        }
                    }
                    if (columnType.matches("(.*)LocalDateTime")) {

                        try {
                            ps.setObject(count.get(), md.getGetter(f, object));
                        } catch (SQLException throwables) {
                            throwables.printStackTrace();
                        }
                    }
                    if (columnType.matches("(.*)LocalDate")) {
                        try {
                            ps.setObject(count.get(), md.getGetter(f, object));
                        } catch (SQLException throwables) {
                            throwables.printStackTrace();
                        }
                    }
                    if (columnType.equals("int") || columnType.equals("Integer")) {
                        try {
                            ps.setInt(count.get(), (Integer) md.getGetter(f, object));
                        } catch (SQLException throwables) {
                            throwables.printStackTrace();
                        }
                    }
                    if (columnType.equals("double")) {
                        try {
                            ps.setDouble(count.get(), (Double) md.getGetter(f, object));
                        } catch (SQLException throwables) {
                            throwables.printStackTrace();
                        }
                    }
                    count.getAndIncrement();
                }
            });
            int success = ps.executeUpdate();
            if (success > 0) {
                System.out.println("Update Successful");
                return true;
            }

        } catch (SQLException throwables) {
            System.out.println("Update Failed");
            throwables.printStackTrace();
        }

        return false;
    }

    @Override
    public <T> boolean update(T object, String columnName, T value, int id) {
        Class<?> objectClass = object.getClass();
        String table = objectClass.getSimpleName();

        String sql = "UPDATE " + table + " SET " + columnName + " = ";
        if (value instanceof Integer || value instanceof Double ) {//for strings
            sql += value;
        } else {
            sql += "\'" + value + "\'";
        }
        sql += " WHERE id = " + id;

        try (Connection connection = ConnectionUtil.getConnection();
             Statement statement = connection.createStatement()) {
            statement.execute(sql);
            return true;
        } catch (SQLException throwables) {
            System.out.println("Update Failed");
            throwables.printStackTrace();
        }

        return false;
    }

    /////---------------delete deletes a particular record from table----(DELETE operation)----------------------------------------------------//////
    @Override
    public <T> boolean delete(T object, String selectedColumn, T value) {
        Class<?> objectClass = object.getClass();
        String tableName = objectClass.getSimpleName();

        String sql = "Delete from " + tableName + " WHERE " + selectedColumn + " = ";
        if (value instanceof Integer || value instanceof Double) {//for strings
            sql += value + ";";
        } else {
            sql += "\'" + value + "\';";

        }

        try (Connection connection = ConnectionUtil.getConnection();
             Statement s = connection.createStatement();) {
            s.execute(sql);
            return true;
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return false;
    }


}
