package dev.team6.daos;

public interface DataBaseDao {

    /////--------------------All DDL functions are added in this interface---------------------/////

    public <T> void createTable(T object);

    public <T> boolean dropTable(T object);


}
