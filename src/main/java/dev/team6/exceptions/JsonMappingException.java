package dev.team6.exceptions;

public class JsonMappingException extends RuntimeException{
    public JsonMappingException(){}

    public JsonMappingException(String message){
        super(message);
    }

    public JsonMappingException(String message, Exception e){
        super(message, e);
    }
}
