package dev.team6;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;


import dev.team6.daos.DataBaseDao;
import dev.team6.daos.DataBaseDaoImpl;

public class DataBaseTest {

    private static final DataBaseDao dbDao = new DataBaseDaoImpl();

    @Rule
    public ExpectedException expectedException = ExpectedException.none();

}
