package dev.team6.models;

import java.util.Objects;

public class Employee {

    private int id;
    private String employeeName;
    private String email;

    public Employee() {
    }

    public Employee(String employeeName, String email) {
        this.employeeName = employeeName;
        this.email = email;
    }

    public Employee(int id, String employeeName, String email) {
        this.id = id;
        this.employeeName = employeeName;
        this.email = email;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getEmployeeName() {
        return employeeName;
    }

    public void setEmployeeName(String employeeName) {
        this.employeeName = employeeName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public boolean equals(Object o) {

        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Employee employee = (Employee) o;
        return id == employee.id &&
                Objects.equals(employeeName, employee.employeeName) &&
                Objects.equals(email, employee.email);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, employeeName, email);
    }

    @Override
    public String toString() {
        return "Employee{" +
                "id=" + id +
                ", employeeName='" + employeeName + '\'' +
                ", email='" + email + '\'' +
                '}';
    }
}
