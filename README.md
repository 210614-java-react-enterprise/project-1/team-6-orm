# Custom ORM

## Description

This project creates a custom object relational mapping (ORM) framework, written in Java, which allows for a simplified and SQL-free interaction with a relational data source. Low-level JDBC is completely abstracted away from the developer, allowing them to easily query and persist data to a data source. This makes heavy use of the Java Reflection API in order to support CRUD operations for any entity objects as defined by the developer. A servlet-based web application was used to demonstrate the functionality of the ORM.

## Technologies used
* Java 8
* JUnit
* Apache Maven
* Jackson library (for JSON marshalling/unmarshalling)
* Java EE Servlet API
* PostGreSQL deployed on AWS RDS
* DBeaver


## Web application Repository
[BookShelf](https://gitlab.com/210614-java-react-enterprise/project-1/team-6-webapp)

## Contributors
[Marc Hartley](https://gitlab.com/har09028)

[Carlos Galvan](https://gitlab.com/CarlosGalvan)

[Rensy Aikara](https://gitlab.com/RensyAikara)

